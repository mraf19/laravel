@extends('master')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Tambah Data Pemain</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
    <div>
    <h2>Tambah Data</h2>
        <form action="/casts/store" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Bio">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah Data Pemain</button>
        </form>
    </div>
    </div>
   
</div>

@endsection