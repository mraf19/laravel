@extends('master')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Detail Pemain</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <h2>Detail Pemain {{$cast->id}}</h2>
        <ul>
            <li>{{$cast->nama}}</li>
            <li>{{$cast->umur}}</li>
            <li>{{$cast->bio}}</li>
        </ul>
    </div>
    <!-- /.card-body -->
    <!-- <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
    </div> -->
</div>

@endsection