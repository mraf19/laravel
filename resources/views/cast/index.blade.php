@extends('master')
@section('content')
<a href="casts/create" class="btn btn-primary ml-3 mt-3 mb-3">  Tambah Data Pemain</a>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Data Pemain</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th style="width: 40px">Label</th>
                </tr>
            </thead>
            <tbody>
            @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td style="display: flex;">
                            <a href="/casts/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/casts/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                            <form action="/posts/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="5" align="center">
                        <td >No data</td>
                    </tr>  
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <!-- <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
    </div> -->
</div>

@endsection