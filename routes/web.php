<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/casts', 'CastController@index');
Route::get('/casts/create', 'CastController@create');

Route::post('/casts/store', 'CastController@store');
Route::get('/casts/{cast_id}', 'CastController@show');

Route::get('/casts/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{id}', 'CastController@destroy');